variable project_name {
  type = string
  description = "resource group name"
  default = "WeightTracker"
}

variable resource_group_abbv {
  type = string 
  description = "resource group abrevaition"
  default= "rg"
}


variable "resource_group_name" {
  type        = string
  description = "Name of the Azure resource group"
}

variable "virtual_network_name" {
  type        = string
  description = "Name of the Azure virtual network"
}

variable "subnet_web_name" {
  type        = string
  description = "Name of the web subnet"
}

variable "subnet_db_name" {
  type        = string
  description = "Name of the database subnet"
}

variable "nsg_web_name" {
  type        = string
  description = "Name of the web network security group"
}

variable "nsg_db_name" {
  type        = string
  description = "Name of the database network security group"
}




variable "web_ssh_public_key" {
  type        = string
  description = "SSH public key for web VM"
}

variable "db_ssh_public_key" {
  type        = string
  description = "SSH public key for database VM"
}

variable "db_password" {
  type        = string
  description = "Password for the database user"
}
